package main

import "fmt"
import "runtime"

func main() {
	func1()
}

func sendMessage(ch chan<- string) {
	fmt.Println("sendMessage()")
	for i := 0; i < 20; i++ {
		fmt.Println("	sendingMessage", i)
		ch <- fmt.Sprintf("data %d", i)
		fmt.Println("	message sent", i)
	}
	close(ch)
}

func printMessage(ch <-chan string) {
	fmt.Println("printMessage()")
	for message := range ch {
		fmt.Println(message)
	}
}

// ask
// penerapan for - range - close pada channel
func func1() {
	fmt.Println("func1()...")

	runtime.GOMAXPROCS(2)

	var messages = make(chan string)
	go sendMessage(messages)
	printMessage(messages)
}
