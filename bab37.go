package main

import "fmt"

func main() {
	func1()
}

type student struct {
	name        string
	height      float64
	age         int32
	isGraduated bool
	hobbies     []string
}

var data = student{
	name:        "wick",
	height:      182.5,
	age:         26,
	isGraduated: false,
	hobbies:     []string{"eating", "sleeping"},
}

// layout format string
func func1() {
	fmt.Println("func1()...")

	// layout format %c
	// format data numeric menjadi karakter unicode
	fmt.Print("%c : 1400 -> ")
	fmt.Printf("%c\n", 1400)
	fmt.Print("%c : 1235 -> ")
	fmt.Printf("%c\n", 1235)

	// layout format %d
	// format data numerik menjadi string numerik berbasis 10
	fmt.Print("%d : data.age -> ")
	fmt.Printf("%d\n", data.age)

	// layout format %e atau %E
	// format numerik desimal menjadi notasi numerik standar scientific notation
	fmt.Print("%e : data.height -> ")
	fmt.Printf("%e\n", data.height)
	fmt.Print("%E : data.height -> ")
	fmt.Printf("%E\n", data.height)

	// layout format %f atau %F
	fmt.Print("%f : data.height -> ")
	fmt.Printf("%f\n", data.height)
	fmt.Print("%.9f : data.height -> ")
	fmt.Printf("%.9f\n", data.height)
	fmt.Print("%.2f : data.height -> ")
	fmt.Printf("%.2f\n", data.height)
	fmt.Print("%.f : data.height -> ")
	fmt.Printf("%.f\n", data.height)

	// layout format %g atau %G
	// format data kapasitas numerik desimal sesuai dengan data
	fmt.Print("%e : 0.123123123123 -> ")
	fmt.Printf("%e\n", 0.123123123123)
	fmt.Print("%f : 0.123123123123 -> ")
	fmt.Printf("%f\n", 0.123123123123)
	fmt.Print("%g : 0.123123123123 -> ")
	fmt.Printf("%g\n", 0.123123123123)
	fmt.Print("%g : 0.12 -> ")
	fmt.Printf("%g\n", 0.12)
	fmt.Print("%.5g : 0.12 -> ")
	fmt.Printf("%.5g\n", 0.12)

	// layout format %o
	// format numerik menjadi string numerik berbasis 8 (oktal)
	fmt.Print("%o : data.age -> ")
	fmt.Printf("%o\n", data.age)

	// layout format %p
	// format data pointer mengembalikan nilai alamat pointer
	fmt.Print("%p : &data.name -> ")
	fmt.Printf("%p\n", &data.name)

	// layout format %q
	// untuk escape string
	fmt.Print("%q  : `\" name \\ height \"` -> ")
	fmt.Printf("%q\n", `" name \ height "`)

	// layout format %s, untuk format string
	fmt.Print("%s : data.name -> ")
	fmt.Printf("%s\n", data.name)

	// layout format %t
	// format data boolean
	fmt.Print("%t : data.isGraduated -> ")
	fmt.Printf("%t\n", data.isGraduated)

	// layout format %T
	// mengambil tipe data yang akan diformat
	fmt.Print("%T : data.name -> ")
	fmt.Printf("%T\n", data.name)
	fmt.Print("%T : data.height -> ")
	fmt.Printf("%T\n", data.height)
	fmt.Print("%T : data.age -> ")
	fmt.Printf("%T\n", data.age)
	fmt.Print("%T : data.isGraduated -> ")
	fmt.Printf("%T\n", data.isGraduated)
	fmt.Print("%T : data.hobbies -> ")
	fmt.Printf("%T\n", data.hobbies)

	// layout format %v
	// format data apa saja
	fmt.Print("%v : data -> ")
	fmt.Printf("%v\n", data)

	// layout format %+v
	// jika struct akan mengembalikan nama dan nilai tiap tipe berurut
	fmt.Print("%+v : data -> ")
	fmt.Printf("%+v\n", data)

	// layout format %#v
	// jika struct, akan mengembalikan nama & nilai tiap tipe
	// dan bagaimana objek tersebut dideklarasikan
	fmt.Print("%#v : data -> ")
	fmt.Printf("%#v\n", data)

	var dataInner = struct {
		name   string
		height float64
	}{
		name:   "wick",
		height: 182.5,
	}
	fmt.Print("%#v : dataInner -> ")
	fmt.Printf("%#v\n", dataInner)

	// layout format %x atau %X
	// %X mengembalikan dalam bentuk uppercase
	// format numerik menjadi string numerik basis 16 (hexadecimal)
	fmt.Print("%x : data.age -> ")
	fmt.Printf("%x\n", data.age)

	var d = data.name
	fmt.Println("d :", d)
	fmt.Print("%X%x%x%x : d[0], d[1], d[2], d[3] -> ")
	fmt.Printf("%X%x%x%x\n", d[0], d[1], d[2], d[3])
	fmt.Print("%x : d -> ")
	fmt.Printf("%x\n", d)

	// layout format %%
	// cara menulis karakter % pada string format
	fmt.Print("%% -> ")
	fmt.Printf("%%\n")
}
