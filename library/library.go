package library

import (
	"fmt"
)

// SayHello sayhello
func SayHello(name string) {
	fmt.Println("hello")
	introduce(name)
}

func introduce(name string) {
	fmt.Println("nama saya", name)
}

// Student student
type Student struct {
	Name  string
	Grade int
}

// StudentVar variable
var StudentVar = struct {
	Name  string
	Grade int
}{}

// fungsi init()
func init() {
	StudentVar.Name = "john wick"
	StudentVar.Grade = 2

	fmt.Println("--> library/library.go imported")
}
