package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
	func6()
}

// perulangan menggunakan keyword for
func func1() {
	fmt.Println("func1()...")

	for i := 0; i < 5; i++ {
		fmt.Println("Angka", i)
	}
}

// perulangan keyword for dengan argumen hanya kondisi
func func2() {
	fmt.Println("\nfunc2()...")

	var i = 0

	for i < 5 {
		fmt.Println("Angka", i)
		i++
	}
}

// penggunaan keyword for tanpa argumen
func func3() {
	fmt.Println("\nfunc3()...")

	var i = 0

	for {
		fmt.Println("Angka", i)

		i++
		if i == 5 {
			break
		}
	}
}

// penggunaan keyword break & continue
func func4() {
	fmt.Println("\nfunc4()...")

	for i := 1; i <= 10; i++ {
		if i%2 == 1 {
			continue
		}

		if i > 8 {
			break
		}

		fmt.Println("Angka", i)
	}
}

// perulangan bersarang
func func5() {
	fmt.Println("\nfunc5()...")

	for i := 0; i < 5; i++ {
		for j := i; j < 5; j++ {
			fmt.Print(j, " ")
		}

		fmt.Println()
	}
}

// pemanfaatan label dalam perulangan
func func6() {
	fmt.Println("\nfunc6()...")

outerLoop:
	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			if i == 3 {
				break outerLoop
			}

			fmt.Print("matriks [", i, "][", j, "]", "\n")
		}
	}
}
