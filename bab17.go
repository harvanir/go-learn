package main

import (
	"fmt"
	"math/rand"
	"time"
)
import "strings"

func main() {
	func1()
	func2()
}

// penerapan fungsi
func func1() {
	fmt.Println("func1()...")
	var names = []string{"john", "wick"}
	printMessage("halo", names)
}

func printMessage(message string, arr []string) {
	var nameString = strings.Join(arr, " ")
	fmt.Println(message, nameString)
}

// fungsi dengan return value / nilai balik
func func2() {
	fmt.Println("\nfunc2()...")
	rand.Seed(time.Now().Unix())
	var randomValue int

	randomValue = randomWithRange(2, 10)
	fmt.Println("random number:", randomValue)
	fmt.Println()
	randomValue = randomWithRange(2, 10)
	fmt.Println("random number:", randomValue)
	fmt.Println()
	randomValue = randomWithRange(2, 10)
	fmt.Println("random number:", randomValue)
}

func randomWithRange(min, max int) int {
	var random = rand.Int()
	var maxMinusMinPlusOne = max - min + 1

	fmt.Println("random:", random)
	fmt.Printf("%d %s %d: %d\n", random, "%", maxMinusMinPlusOne, random%maxMinusMinPlusOne)

	var value = random%maxMinusMinPlusOne + min
	return value
}
