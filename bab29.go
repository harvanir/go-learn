package main

import "fmt"
import "runtime"

func main() {
	func1()
}

func print(till int, message string) {
	for i := 0; i < till; i++ {
		fmt.Println((i + 1), message)
	}
}

// penerapan goroutine
func func1() {
	fmt.Println("func1()...")

	runtime.GOMAXPROCS(2)

	go print(5, "halo")
	print(5, "apa kabar")

	var args1, args2, args3 string
	fmt.Scanln(&args1, &args2, &args3)

	fmt.Println("args1	:", args1)
	fmt.Println("args2	:", args2)
	fmt.Println("args3	:", args3)
}
