package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
}

// Penggunaan konstanta
func func1() {
	const firstName string = "john"
	fmt.Println("\nfunc1()...")
	fmt.Print("halo ", firstName, "!\n")

	const lastName = "wick"
	fmt.Print("nice to meet you ", lastName, "!\n")
}

// Penggunaan Fungsi fmt.Print()
func func2() {
	fmt.Println("\nfunc2()...")
	fmt.Println("john wick")
	fmt.Println("john", "wick")

	fmt.Print("john wick\n")
	fmt.Print("john ", "wick\n")
	fmt.Print("john", " ", "wick\n")
}
