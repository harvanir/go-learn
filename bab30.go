package main

import (
	"fmt"
	"runtime"
)

func main() {
	func1()
	func2()
	func3()
}

// penerapan channel
func func1() {
	runtime.GOMAXPROCS(3)
	fmt.Println("func1()...")

	var messages = make(chan string)

	var sayHelloTo = func(who string) {
		var data = fmt.Sprintf("hello %s", who)
		messages <- data
	}

	go sayHelloTo("john wick")
	go sayHelloTo("ethan hunt")
	go sayHelloTo("jason bourne")

	var message1 = <-messages
	fmt.Println(message1)

	var message2 = <-messages
	fmt.Println(message2)

	var message3 = <-messages
	fmt.Println(message3)
}

func printMessage(what chan string) {
	fmt.Println(<-what)
}

// channel sebagai tipe data parameter
func func2() {
	fmt.Println("\nfunc2()...")

	runtime.GOMAXPROCS(3)

	var messages = make(chan string)

	for _, each := range []string{"wick", "hunt", "bourne"} {
		go func(who string) {
			var data = fmt.Sprintf("hello %s", who)
			messages <- data
		}(each)
	}

	for i := 0; i < 3; i++ {
		printMessage(messages)
	}
}

// eksekusi goroutine pada iife
func func3() {
	fmt.Println("\nfunc3()...")

	var messages = make(chan string)

	go func(who string) {
		var data = fmt.Sprintf("hello IIFE %s", who)
		messages <- data
	}("wick")

	var message = <-messages
	fmt.Println(message)
}
