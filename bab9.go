package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
	func3()
	func4()
}

// Tipe data numerik non-desimal
func func1() {
	var positiveNumber uint8 = 89
	var negativeNumber = -1243423644

	fmt.Println("\nfunc1()...")
	fmt.Printf("bilangan positif: %d\n", positiveNumber)
	fmt.Printf("bilangan negatif: %d\n", negativeNumber)
}

// Tipe data numerik desimal
func func2() {
	var decimalNumber = 2.62

	fmt.Println("\nfunc2()...")
	fmt.Printf("bilangan desimal: %f\n", decimalNumber)
	fmt.Printf("bilangan desimal: %.3f\n", decimalNumber)
}

// Tipe data bool (Boolean)
func func3() {
	var exist bool = true

	fmt.Println("\nfunc3()...")
	fmt.Printf("exist? %t \n", exist)
}

// Tipe data string
func func4() {
	var message string = "Halo"

	fmt.Println("\nfunc4()...")
	fmt.Printf("message: %s \n", message)

	func4_1()
}

// Tide pada string dengan assignment tanda `
func func4_1() {
	var message string = `Nama saya "John Wick".
Salam kenal.
Mari belajar "Golang".`

	fmt.Println(message)
}
