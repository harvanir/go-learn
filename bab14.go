package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
	func6()
	func7()
	func8()
	func9()
	func10()
}

// array
func func1() {
	fmt.Println("func1()...")

	var names [4]string
	names[0] = "names[0]"
	names[1] = "names[1]"
	names[2] = "names[2]"
	names[3] = "names[3]"

	fmt.Println(names[0], names[1], names[2], names[3])
}

// pengisian elemen array yang melebihi alokasi awal
func func2() {
	fmt.Println("\nfunc2()...")

	var names [4]string
	names[0] = "names[0]"
	names[1] = "names[1]"
	names[2] = "names[2]"
	names[3] = "names[3]"

	//names[4] = "names[4]" // baris kode ini menghasilkan error
}

// inisialisasi nilai awal array
func func3() {
	fmt.Println("\nfunc3()...")

	var fruits = [4]string{"apple", "grape", "banana", "melon"}

	fmt.Println("Jumlah element \t\t", len(fruits))
	fmt.Println("isi semua element \t", fruits)
}

// inisialisasi nilai array dengan gaya vertikal
func func4() {
	fmt.Println("\nfunc4()...")

	var fruits [4]string

	// cara horizontal
	fruits = [4]string{"apple", "grape", "banana", "melon"}

	fmt.Println("fruits:\t", fruits)

	fruits = [4]string{
		"apple",
		"grape",
		"banana",
		"melon",
	}
	fmt.Println("fruits:\t", fruits)
}

// inisialisasi nilai awal array tanpa jumlah elemen
func func5() {
	fmt.Println("\nfunc5()...")

	var numbers = [...]int{1, 2, 3, 4, 5}

	fmt.Println("data array \t:", numbers)
	fmt.Println("jumlah element \t:", len(numbers))
}

// array multidimensi
func func6() {
	fmt.Println("\nfunc6()...")

	var numbers1 = [2][3]int{[3]int{1, 2, 3}, [3]int{3, 2, 1}}
	var numbers2 = [2][3]int{{1, 2, 3}, {3, 2, 1}}
	var numbers3 = [][]int{{1, 2, 3}, {3, 2, 1}}

	fmt.Println("numbers1", numbers1)
	fmt.Println("numbers2", numbers2)
	fmt.Println("numbers3", numbers3)
}

// perulangan elemen array menggunakan keyword for
func func7() {
	fmt.Println("\nfunc7()...")

	var fruits = [4]string{"apple", "grape", "banana", "melon"}

	for i := 0; i < len(fruits); i++ {
		fmt.Printf("element %d : %s\n", i, fruits[i])
	}
}

// perulangan elemen array menggunakan keyword for - range
func func8() {
	fmt.Println("\nfunc8()...")

	var fruits = [4]string{"apple", "grape", "banana", "melon"}

	for i, fruit := range fruits {
		fmt.Printf("element %d : %s\n", i, fruit)
	}
}

// penggunaan variable underscore _ dalam for - range
func func9() {
	fmt.Println("\nfunc9()...")

	var fruits = [4]string{"apple", "grape", "banana", "melon"}

	for _, fruit := range fruits {
		fmt.Printf("nama buah : %s\n", fruit)
	}

	for i, _ := range fruits {
		fmt.Printf("buah index ke %d\n", i)
	}
}

// alokasi elemen array menggunakan keyword make
func func10() {
	fmt.Println("\nfunc10()...")

	var fruits = make([]string, 2)
	fruits[0] = "apple"
	fruits[1] = "manggo"

	fmt.Println(fruits) // [apple manggo]
}
