package main

import (
	"fmt"
	"runtime"
)

func main() {
	func1()
}

func func1() {
	fmt.Println("func1()...")

	runtime.GOMAXPROCS(2)

	messages := make(chan int, 2)

	go func() {
		for {
			i := <-messages
			fmt.Println("receive data", i)
		}
	}()

	for i := 0; i < 12; i++ {
		fmt.Println("send data", i)
		messages <- i
	}

	var args1 string
	fmt.Scanln(&args1)
	fmt.Println(args1)
}
