package main

import (
	"fmt"
	"strings"
)

// interface kosong
func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
}

// penggunaan interface{}
func func1() {
	fmt.Println("func1()...")
	var secret interface{}

	secret = "ethan hunt"
	fmt.Println(secret)

	secret = []string{"apple", "manggo", "banana"}
	fmt.Println(secret)

	secret = 12.4
	fmt.Println(secret)
}

// contoh 2
func func2() {
	fmt.Println("\nfunc2()...")

	var data map[string]interface{}

	data = map[string]interface{}{
		"name":      "ethan hunt",
		"grade":     2,
		"breakfast": []string{"apple", "manggo", "banana"},
	}

	fmt.Println("data:", data)
}

// casting variabel interface kosong
func func3() {
	fmt.Println("\nfunc3()...")

	var secret interface{}

	secret = 2
	var number = secret.(int) * 10
	fmt.Println(secret, "multiplied by 10 is :", number)

	secret = []string{"apple", "manggo", "banana"}
	var gruits = strings.Join(secret.([]string), ", ")
	fmt.Println(gruits, "is my favorite fruits")
}

type person struct {
	name string
	age  int
}

// casting variabel interface kosong ke objek pointer
func func4() {
	fmt.Println("\nfunc4()...")

	var secret interface{} = &person{name: "wick", age: 27}
	var name = secret.(*person).name
	fmt.Println("secret.name:", name)
}

// kombinasi slice, map, dan interface{}
func func5() {
	fmt.Println("\nfunc5()...")

	var person = []map[string]interface{}{
		{"name": "wick", "age": 23},
		{"name": "ethan", "age": 24},
		{"name": "bourne", "age": "25"},
	}

	for _, each := range person {
		fmt.Println(each["name"], "age is", each["age"])
	}

	fmt.Println("")

	var fruits = []interface{}{
		map[string]interface{}{"name": "strawberry", "total": 10},
		[]string{"manggo", "pineapple", "papaya"},
		"orange",
	}

	for _, each := range fruits {
		fmt.Println("each:", each)
	}
}
