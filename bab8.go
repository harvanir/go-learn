package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
}

// Deklarasi variabel dengan tipe data
// Deklarasi variabel menggunakan keywoard var
// Penggunaan Fungsi fmt.Printf()
func func1() {
	var firstName string = "john"

	var lastName string
	lastName = "wick"

	fmt.Println("func1()...")
	fmt.Printf("halo john wick!\n")
	fmt.Printf("halo %s %s!\n", firstName, lastName)
	fmt.Println("halo", firstName, lastName+"!")
}

// Deklarasi Variabel Tanpa Tipe Data
func func2() {
	var format string = "halo %s %s!\n"
	var firstName string = "john"
	lastName := "wick"

	fmt.Println("\nfunc2()...")
	fmt.Printf(format, firstName, lastName)

	lastName = "ethan"
	fmt.Printf(format, firstName, lastName)
}

// Deklarasi Multi Variabel
func func3() {
	var first, second, third string
	first, second, third = "satu", "dua", "tiga"

	var fourth, fifth, sixth string = "empat", "lima", "enam"

	seventh, eight, ninth := "tujuh", "delapan", "sembilan"

	one, isFriday, twoPointTwo, say := 1, true, 2.2, "hello"

	fmt.Println("\nfunc3()...")
	fmt.Println(first, second, third, fourth, fifth, sixth, seventh, eight, ninth, one, isFriday, twoPointTwo, say)
}

// Variabel Underscore _
func func4() {
	_ = "Belajar Golang"
	_ = "Golang itu mudah"
	name, _ := "john", "wick"

	fmt.Println("\nfunc4()...")
	fmt.Println(name)
}

func func5() {
	name := new(string)

	fmt.Println("\nfunc5()...")
	fmt.Println(name)
	fmt.Println(*name)
}
