package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
	func6()
	func7()
	func8()
	func9()
	func10()
	func11()
}

// inisialisasi slice
func func1() {
	fmt.Println("func1()...")

	var fruits = []string{"apple", "grape", "banana", "melon"}
	fmt.Println(fruits[0])

	var fruitsA = []string{"apple", "grape"}     // slice
	var fruitsB = [2]string{"banana", "melon"}   // array
	var fruitsC = [...]string{"papaya", "grape"} // array

	fmt.Println("fruitsA (slice):", fruitsA)
	fmt.Println("fruitsB (array):", fruitsB)
	fmt.Println("fruitsC (array):", fruitsC)
}

// hubungan slice dengan array & operasi slice
func func2() {
	fmt.Println("\nfunc2()...")

	var fruits = []string{"apple", "grape", "banana", "melon"}
	var newFruits = fruits[0:2]

	fmt.Println(newFruits)
}

// test akses slice vs array
func func3() {
	fmt.Println("\nfunc3()...")

	var fruits = [4]string{"apple", "grape", "banana", "melon"}
	var newFruits = fruits[0:2]

	fmt.Println(newFruits)
}

// slice merupakan tipe data reference
func func4() {
	fmt.Println("\nfunc4()...")

	var fruits = []string{"apple", "grape", "banana", "melon"}

	var aFruits = fruits[0:3]
	var bFruits = fruits[1:4]

	var aaFruits = aFruits[1:2]
	var baFruits = bFruits[0:1]

	fmt.Println(fruits)
	fmt.Println("aFruits : fruits[0:3]", aFruits)
	fmt.Println("bFruits : fruits[1:4]", bFruits)
	fmt.Println("aaFruits : aFruits[1:2]", aaFruits)
	fmt.Println("baFruits : bFruits[0:1]", baFruits)

	//baFruits[0] = "pinnaple"
	fruits[1] = "pinnaple"

	fmt.Println(fruits)   // [apple pinnaple banana melon]
	fmt.Println(aFruits)  // [apple pinnaple banana]
	fmt.Println(bFruits)  // [pinnaple banana melon]
	fmt.Println(aaFruits) // [pinnaple]
	fmt.Println(baFruits) // [pinnaple]
}

// test slice vs array
func func5() {
	fmt.Println("\nfunc5()...")

	var fruits = [4]string{"apple", "grape", "banana", "melon"}

	var aFruits = fruits[0:3]
	var bFruits = fruits[1:4]

	var aaFruits = aFruits[1:2]
	var baFruits = bFruits[0:1]

	fmt.Println(fruits)
	fmt.Println("aFruits : fruits[0:3]", aFruits)
	fmt.Println("bFruits : fruits[1:4]", bFruits)
	fmt.Println("aaFruits : aFruits[1:2]", aaFruits)
	fmt.Println("baFruits : bFruits[0:1]", baFruits)

	//baFruits[0] = "pinnaple"
	fruits[1] = "pinnaple"

	fmt.Println(fruits)   // [apple pinnaple banana melon]
	fmt.Println(aFruits)  // [apple pinnaple banana]
	fmt.Println(bFruits)  // [pinnaple banana melon]
	fmt.Println(aaFruits) // [pinnaple]
	fmt.Println(baFruits) // [pinnaple]
}

// fungsi len()
func func6() {
	fmt.Println("\nfunc6()...")

	var fruits = []string{"apple", "grape", "banana", "melon"}
	fmt.Println(len(fruits))
}

// fungsi cap()
func func7() {
	fmt.Println("\nfunc7()...")

	var fruits = []string{"apple", "grape", "banana", "melon"}
	fmt.Println(len(fruits)) // len: 4
	fmt.Println(cap(fruits)) // cap: 4

	var aFruits = fruits[0:3]
	fmt.Println(len(aFruits)) // len: 3

	// cap: 4
	// why? because any slice starting at index 0 will have the same cap from reference
	fmt.Println(cap(aFruits))

	var bFruits = fruits[1:4]
	fmt.Println(len(bFruits)) // len: 3
	fmt.Println(cap(bFruits)) // cap: 3

}

// fungsi append()
func func8() {
	fmt.Println("\nfunc8()...")

	var fruits = []string{"apple", "grape", "banana"}
	var cFruits = append(fruits, "papaya")

	fmt.Println(fruits)
	fmt.Println(cFruits)
}

// detil append()
func func9() {
	fmt.Println("\nfunc9()...")

	var fruits = []string{"apple", "grape", "banana"}
	var bFruits = fruits[0:2]

	fmt.Println("cap(bFruits)", cap(bFruits)) // 3
	fmt.Println("len(bFruits)", len(bFruits)) // 2

	fmt.Println("fruits", fruits)   // [apple grape banana]
	fmt.Println("bFruits", bFruits) // [apple grape]

	//var cFruits = append(append(bFruits, "papaya"), "orange")
	var cFruits = append(bFruits, "papaya")
	fmt.Println("cap(cFruits)", cap(cFruits)) // 3
	fmt.Println("len(cFruits)", len(cFruits)) // 3

	fruits[0] = "newApple1"
	fmt.Println("fruits", fruits)   // [newApple1 grape papaya], value on last index changed
	fmt.Println("bFruits", bFruits) // [newApple1 grape]
	fmt.Println("cFruits", cFruits) // [newApple1 grape papaya]

	cFruits = append(cFruits, "orange")
	fruits[0] = "newApple2"

	fmt.Println("cap(cFruits)", cap(cFruits)) // 6
	fmt.Println("len(cFruits)", len(cFruits)) // 4

	fmt.Println("fruits", fruits)   // [newApple2 grape papaya]
	fmt.Println("bFruits", bFruits) // [newApple2 grape]

	// [newApple1 grape papaya orange], because len(ref) == cap(ref),
	// cFruit create newly reference
	// so changes reference value will won't impact the newly reference
	fmt.Println("cFruits", cFruits)
}

// fungsi copy()
func func10() {
	fmt.Println("\nfunc10")

	var fruits = []string{"apple"}
	fmt.Println(fruits)

	var aFruits = []string{"watermelon", "pinnaple"}

	var copiedElemen = copy(fruits, aFruits)

	fmt.Println(fruits)
	fmt.Println(aFruits)
	fmt.Println(copiedElemen)
}

// pengaksesan elemen slice dengan 3 indeks
func func11() {
	fmt.Println("\nfunc11()...")

	var fruits = []string{"apple", "grape", "banana"}
	var aFruits = fruits[0:2]
	var bFruits = fruits[0:2:2]

	fmt.Println("fruits", fruits)
	fmt.Println("len(fruits)", len(fruits))
	fmt.Println("cap(fruits)", cap(fruits))

	fmt.Println("aFruits", aFruits)
	fmt.Println("len(aFruits)", len(aFruits))
	fmt.Println("cap(aFruits)", cap(aFruits))

	fmt.Println("bFruits", bFruits)
	fmt.Println("len(bFruits)", len(bFruits))
	fmt.Println("cap(bFruits)", cap(bFruits))
}
