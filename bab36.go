package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

func main() {
	func1()
	func2()
	func3()
	func4()
}

// pemanfaatan error
func func1() {
	fmt.Println("func1()...")

	var input string
	fmt.Print("type some number: ")
	fmt.Scanln(&input)

	var number int
	var err error
	number, err = strconv.Atoi(input)

	if err == nil {
		fmt.Println(number, "is number")
	} else {
		fmt.Println(input, "is not number")
		fmt.Println(err.Error())
	}
}

// membuat custom error
func func2() {
	fmt.Println("\nfunc2()...")

	var name string
	fmt.Println("type your name: ")
	fmt.Scanln(&name)

	if valid, err := validate(name); valid {
		fmt.Println("halo", name)
	} else {
		fmt.Println(err.Error())
	}
}

func validate(input string) (bool, error) {
	if strings.TrimSpace(input) == "" {
		return false, errors.New("cannot be emtpy")
	}
	return true, nil
}

// penggunaan panic
func func3() {
	fmt.Println("\nfunc3()...")

	var name string
	fmt.Println("type your name panic: ")
	fmt.Scanln(&name)

	if valid, err := validate(name); valid {
		fmt.Println("halo", name)
	} else {
		panic(err.Error())
		fmt.Println("end")
	}
}

// penggunaan recover
func func4() {
	fmt.Println("\nfunc4()...")

	defer catch()

	var name string
	fmt.Println("type your name recover: ")
	fmt.Scanln(&name)

	if valid, err := validate(name); valid {
		fmt.Println("halo", name)
	} else {
		panic(err.Error())
		fmt.Println("end")
	}
}

func catch() {
	if r := recover(); r != nil {
		fmt.Println("error occured:", r)
	} else {
		fmt.Println("application running perfectly")
	}
}
