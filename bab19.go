package main

import (
	"fmt"
	"strings"
)

func main() {
	func1()
	func2()
}

// penerapan fungsi variadic
func func1() {
	fmt.Println("func1()...")

	var avg = calculate(2, 4, 3, 5, 4, 3, 3, 5, 3)
	var msg = fmt.Sprintf("Rata-rata : %.2f", avg)
	fmt.Println(msg)
}

func calculate(numbers ...int) float64 {
	var total int
	for _, number := range numbers {
		total += number
	}

	var avg = float64(total) / float64(len(numbers))
	return avg
}

// fungsi dengan parameter biasa & variadic
func func2() {
	fmt.Println("\nfunc2()...")

	yourHobbies("wick", "sleeping", "eating")

	var hobbies = []string{"sleeping", "eating"}
	yourHobbies("wick", hobbies...)

	yourHobbies("wick", []string{"sleeping", "eating"}...)
}

func yourHobbies(name string, hobbies ...string) {
	var hobbiesAsString = strings.Join(hobbies, ", ")

	fmt.Printf("Hello, my name is: %s\n", name)
	fmt.Printf("My hobbies are: %s\n", hobbiesAsString)
}
