package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
	func6()
}

// penggunaan map
func func1() {
	fmt.Println("func1()...")

	var chicken map[string]int
	chicken = map[string]int{}

	chicken["januari"] = 50
	chicken["februari"] = 40

	fmt.Println("januari", chicken["januari"])
	fmt.Println("mei", chicken["mei"])
}

// inisialisasi map
func func2() {
	fmt.Println("\nfunc2()...")

	// cara vertikal
	var chicken1 = map[string]int{"januari": 50, "februari": 40}
	fmt.Println("chicken1", chicken1)

	// cara horizontal
	var chicken2 = map[string]int{
		"januari":  50,
		"februari": 40,
	}
	fmt.Println("chicken2", chicken2)

	var chicken3 = map[string]int{"one": 1, "two": 2}
	fmt.Println("chicken3", chicken3)

	var chicken4 = make(map[string]int)
	chicken4["one"] = 1
	chicken4["two"] = 2
	fmt.Println("chicken4", chicken4)

	var chicken5 = *new(map[string]int)
	fmt.Println("chiecken5", chicken5)
}

// iterasi item map menggunakan for - range
func func3() {
	fmt.Println("\nfunc3()...")

	var chicken = map[string]int{
		"januari":  50,
		"februari": 40,
		"maret":    34,
		"april":    67,
	}

	for key, val := range chicken {
		fmt.Println(key, "\t:", val)
	}
}

// menghapus item map
func func4() {
	fmt.Println("\nfunc4()...")

	var chicken = map[string]int{"januari": 50, "februari": 40}

	fmt.Println("len(chicken)", len(chicken))
	fmt.Println("chicken", chicken)

	fmt.Println("deleting chicken[\"januari\"]")
	delete(chicken, "januari")

	fmt.Println("len(chicken)", len(chicken))
	fmt.Println("chicken", chicken)
}

// deteksi keberadaan item dengan key tertentu
func func5() {
	fmt.Println("\nfunc5()...")

	var chicken = map[string]int{"januari": 50, "februari": 40}
	var value, isExist = chicken["mei"]

	if isExist {
		fmt.Println(value)
	} else {
		fmt.Println("item is not exists")
	}
}

// kombinasi slice & map
func func6() {
	fmt.Println("\nfunc6()...")

	fmt.Println()
	var chickens = []map[string]string{
		map[string]string{"name": "chicken blue", "gender": "male"},
		map[string]string{"name": "chicken red", "gender": "male"},
		map[string]string{"name": "chicken yellow", "gender": "female"},
	}
	for _, chicken := range chickens {
		fmt.Println(chicken["name"], chicken["gender"])
	}

	fmt.Println()
	chickens = []map[string]string{
		{"name": "chicken blue", "gender": "male"},
		{"name": "chicken red", "gender": "male"},
		{"name": "chicken yellow", "gender": "female"},
	}
	for _, chicken := range chickens {
		fmt.Println(chicken["name"], chicken["gender"])
	}

	fmt.Println()
	var datas = []map[string]string{
		{"name": "chicken blue", "gender": "male", "color": "brown"},
		{"address": "mangga street", "id": "K001"},
		{"community": "chicken lovers"},
	}
	for _, data := range datas {
		fmt.Println(data["name"], data["gender"], data["color"], data["address"], data["id"], data["community"])
	}
}
