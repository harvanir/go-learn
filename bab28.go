package main

import (
	"fmt"
	"reflect"
	"time"
)

func main() {
	func1()
	func2()
	func3()
	func4()
}

// mencari tipe data & value menggunakan reflect
func func1() {
	fmt.Println("func1()...")

	var number = 23
	var reflectValue = reflect.ValueOf(number)

	fmt.Println("tipe variabel:", reflectValue.Type())

	if reflectValue.Kind() == reflect.Int {
		fmt.Println("nilai variabel :", reflectValue.Int())
	}
}

// pengaksesan nilai dalam bentuk interface{}
func func2() {
	fmt.Println("\nfunc2()...")

	var number = 23
	var reflectValue = reflect.ValueOf(number)

	fmt.Println("tipe variabel	:", reflectValue.Type())
	fmt.Println("nilai variabel	:", reflectValue.Interface())
	fmt.Println("reflect value	:", reflectValue.Interface().(int))
}

type student struct {
	Name  string
	Grade int
}

func (s *student) getPropertyInfo() {
	var reflectValue = reflect.ValueOf(s)
	fmt.Println("reflectValue	:", reflectValue)

	if reflectValue.Kind() == reflect.Ptr {
		reflectValue = reflectValue.Elem()
	}
	fmt.Println("reflectValue	:", reflectValue)

	var reflectType = reflectValue.Type()
	fmt.Println("reflectType	:", reflectType)

	for i := 0; i < reflectValue.NumField(); i++ {
		fmt.Println("nama		:", reflectType.Field(i).Name)
		fmt.Println("tipe data	:", reflectType.Field(i).Type)
		fmt.Println("nilai		:", reflectValue.Field(i).Interface())
	}
}

// pengaksesan informasi property variabel objek
func func3() {
	fmt.Println("\nfunc3()...")

	var s1 = &student{"wick", 2}
	s1.getPropertyInfo()
}

func (s *student) SetName(name string) {
	s.Name = name
}

// pengaksesan informasi method variabel objek
func func4() {
	fmt.Println("\nfunc4()...")

	var s1 = &student{Name: "john wick", Grade: 2}
	fmt.Println("nama :", s1.Name)

	var reflectValue = reflect.ValueOf(s1)
	var method = reflectValue.MethodByName("SetName")
	method.Call([]reflect.Value{
		reflect.ValueOf("wick"),
	})

	fmt.Println("nama :", s1.Name)
	loop := 10000000
	start := time.Now()
	for i := 0; i < loop; i++ {
		method.Call([]reflect.Value{
			reflect.ValueOf("loop1"),
		})
	}
	fmt.Printf("Reflection setter elapsed in %d ms.\n", time.Since(start)/1000000)
	fmt.Println("new name:", s1.Name)
	fmt.Println("")

	start = time.Now()
	for i := 0; i < loop; i++ {
		s1.Name = "loop2"
	}
	fmt.Printf("Native setter elapsed in %d ms.\n", time.Since(start)/1000000)
	fmt.Println("new name:", s1.Name)
}
