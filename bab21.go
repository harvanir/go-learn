package main

import (
	"fmt"
	"strings"
)

func main() {
	func1()
	func2()
}

// penerapan fungsi sebagai parameter
func func1() {
	fmt.Println("func1()...")

	var data = []string{"wick", "jason", "ethan"}
	var dataContainsO = filter(data, func(each string) bool {
		return strings.Contains(each, "o")
	})

	var dataLength5 = filter(data, func(each string) bool {
		return len(each) == 5
	})

	fmt.Println("data asli \t\t:", data)
	fmt.Println("filter ada huruf \"o\"\t:", dataContainsO)
	fmt.Println("filter jumlah huruf \"5\"\t:", dataLength5)
}

func filter(data []string, callback func(string) bool) []string {
	var result []string
	for _, each := range data {
		if filtered := callback(each); filtered {
			result = append(result, each)
		}
	}

	return result
}

// alias skema closure
func func2() {
	fmt.Println("\nfunc2()...")

	var data = []string{"wick", "jason", "ethan"}
	var dataContainsO = filter2(data, filterDataContainsO)
	var dataLength5 = filter2(data, filterDataLength5)

	fmt.Println("data asli \t\t:", data)
	fmt.Println("filter ada huruf \"o\"\t:", dataContainsO)
	fmt.Println("filter jumlah huruf \"5\"\t:", dataLength5)
}

// FilterCallback is a type
type FilterCallback func(string) bool

func filterDataContainsO(value string) bool {
	return strings.Contains(value, "o")
}

func filterDataLength5(value string) bool {
	return len(value) == 5
}

func filter2(data []string, callback FilterCallback) []string {
	var result []string
	for _, each := range data {
		if valid := callback(each); valid {
			result = append(result, each)
		}
	}
	return result
}
