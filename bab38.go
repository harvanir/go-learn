package main

import "fmt"
import "time"

func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
	func6()
}

// penggunaan time.Time
func func1() {
	fmt.Println("func1()...")

	var time1 = time.Now()
	fmt.Printf("time1 %v\n", time1)

	var time2 = time.Date(2018, 03, 31, 23, 48, 0, 0, time.UTC)
	fmt.Printf("time2 %v\n", time2)

	time2 = time.Date(2018, 03, 31, 23, 48, 0, 0, time.Local)
	fmt.Printf("time2 %v\n", time2)
}

// method milik time.Time
func func2() {
	fmt.Println("\nfunc2()...")

	var now = time.Now()
	fmt.Println("year:", now.Year(), "month:", now.Month())
}

// parsing time.Time
func func3() {
	fmt.Println("\nfunc3()...")

	var layoutFormat, value string
	var date time.Time

	// dd : 02
	// d : 2
	// mm : 01
	// m : 1
	// yyyy : 2006
	// yyy : 006
	// yy : 06
	// MMMM : January
	// MMM : Jan
	// 24 jam : 15
	// 12 jam : 03

	layoutFormat = "2006-01-02 15:04:05"
	value = "2015-09-02 08:04:00"
	date, _ = time.Parse(layoutFormat, value)
	fmt.Println(value, "\t->", date.String())

	layoutFormat = "02/01/2006 MST"
	value = "30/09/2015 WIB"
	date, _ = time.Parse(layoutFormat, value)
	fmt.Println(value, "\t\t->", date.String())
}

// predefined layout format untuk parsing time
func func4() {
	fmt.Println("\nfunc4()...")

	var date, _ = time.Parse(time.RFC822, "02 Sep 15 08:00 WIB")
	fmt.Println(date.String())
}

// format time.Time
func func5() {
	fmt.Println("\nfunc5()...")

	var date, _ = time.Parse(time.RFC822, "02 Sep 15 08:00 WIB")

	var dateS1 = date.Format("Monday 02, January 2006 15:04 MST")
	fmt.Println("dateS1", dateS1)

	var dateS2 = date.Format(time.RFC3339)
	fmt.Println("dateS2", dateS2)
}

// handle error parsing time.Time
func func6() {
	fmt.Println("\nfunc6()...")

	var date, err = time.Parse("06 Jan 15", "02 Sep 15 08:00 WIB")

	if err != nil {
		fmt.Println("error", err.Error())
		return
	}

	fmt.Println(date)
}
