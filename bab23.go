package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
	func6()
	func7()
	func8()
	func9()
	func10()
	func11()
	func12()
}

// deklarasi struct
func func1() {
	fmt.Println("func1()...")

	var s1 student
	s1.name = "john wick"
	s1.grade = 2

	fmt.Println("name	:", s1.name)
	fmt.Println("grade	:", s1.grade)
	fmt.Println("student:", s1)
}

type student struct {
	name  string
	grade int
}

// inisialisasi object struct
func func2() {
	fmt.Println("\nfunc2()...")

	var s1 = student{}
	s1.name = "wick"
	s1.grade = 2

	var s2 = student{"ethan", 2}
	var s3 = student{name: "jason"}
	var s4 = student{name: "ethan", grade: 3}
	var s5 = student{grade: 3, name: "dul"}

	fmt.Println("student 1:", s1.name)
	fmt.Println("student 2:", s2.name)
	fmt.Println("student 3:", s3.name)
	fmt.Println("student 4:", s4.name)
	fmt.Println("student 5:", s5.name)
}

// variabel objek pointer
func func3() {
	fmt.Println("\nfunc3()...")

	var s1 = student{name: "wick", grade: 2}
	var s2 = &s1

	fmt.Println("student 1, name:", s1.name)
	fmt.Println("student 2, name:", s2.name)

	fmt.Println("assign pointer s2.name...")
	s2.name = "ethan"
	fmt.Println("student 1, name:", s1.name)
	fmt.Println("student 2, name:", s2.name)
}

// embedded struct
func func4() {
	fmt.Println("\nfunc4()...")

	var s1 = student2{}
	s1.name = "wick"
	s1.age = 21
	s1.grade = 2

	fmt.Println("name	:", s1.name)
	fmt.Println("age	:", s1.age)
	fmt.Println("age	:", s1.person.age)
	fmt.Println("grade	:", s1.grade)
}

type student2 struct {
	grade int
	person
}

type person struct {
	name string
	age  int
}

type student3 struct {
	person
	age   int
	grade int
}

// embedded struct dengan nama property yang sama
func func5() {
	fmt.Println("\nfunc5()...")

	var s1 = student3{}
	s1.name = "wick"
	s1.age = 21
	s1.person.age = 22

	fmt.Println("s1.name\t\t:", s1.name)
	fmt.Println("s1.age\t\t:", s1.age)
	fmt.Println("s1.person.age\t:", s1.person.age)
}

// pengisian nilai sub-struct
func func6() {
	fmt.Println("\nfunc6()...")

	var p1 = person{name: "wick", age: 21}
	var s1 = student2{person: p1, grade: 2}

	fmt.Println("name\t:", s1.name)
	fmt.Println("age\t:", s1.age)
	fmt.Println("grade\t:", s1.grade)
}

// anonymous struct
func func7() {
	fmt.Println("\nfunc7()...")

	// anonymous struct tanpa pengisian property
	var s1 = struct {
		person
		grade int
	}{}

	s1.person = person{"wick", 21}
	s1.grade = 2

	fmt.Println("name\t:", s1.name)
	fmt.Println("age\t:", s1.age)
	fmt.Println("grade\t:", s1.grade)

	fmt.Println()
	// anonymous struct dengan pengisian property
	var s2 = struct {
		person
		grade int
	}{
		person: person{"wick", 21},
		grade:  2,
	}

	fmt.Println("name\t:", s2.name)
	fmt.Println("age\t:", s2.age)
	fmt.Println("grade\t:", s2.grade)
}

// kombinasi slice & struct
func func8() {
	fmt.Println("\nfunc8()...")

	var allStudents = []person{
		{name: "wick", age: 23},
		{name: "ethan", age: 24},
		{name: "bourne", age: 22},
	}

	for _, student := range allStudents {
		fmt.Println("student.name: "+student.name, ", age is:", student.age)
	}
}

// inisialisasi slice anonymous struct
func func9() {
	fmt.Println("\nfunc9()...")

	var allStudents = []struct {
		person
		grade int
	}{
		{person: person{"wick", 21}, grade: 2},
		{person: person{"ethan", 22}, grade: 3},
		{person: person{"bond", 23}, grade: 4},
	}

	for _, student := range allStudents {
		fmt.Println(student)
	}
}

// deklarasi anonymous struct menggunakan keyword var
func func10() {
	fmt.Println("\nfunc10()...")

	var student struct {
		person
		grade int
	}

	student.person = person{"wick", 21}
	student.grade = 2

	fmt.Println("student: ", student)
	fmt.Println("")

	var student2 struct {
		grade int
	}
	student2.grade = 4
	fmt.Println("student2:", student2)
	fmt.Println("")

	var student3 = struct {
		grade int
	}{
		13,
	}
	fmt.Println("student3:", student3)
}

// nested struct
func func11() {
	fmt.Println("\nfunc11()...")

	type student struct {
		person struct {
			name string
			age  int
		}
		grade   int
		hobbies []string
	}

	var student1 = student{
		person:  person{"name", 23},
		grade:   3,
		hobbies: []string{"swimming", "running"},
	}
	fmt.Println("student1:", student1)
}

// deklarasi dan inisialisasi struct secara horizontal
func func12() {
	fmt.Println("\nfunc12()...")

	// tidak bisa deklarasi struct secara horizontal, auto complete
	type person struct {
		name    string
		age     int
		hobbies []string
	}
	var personInst = person{"name", 4, []string{"hobbies1", "hobbies2"}}
	fmt.Println("person instance:", personInst)

	var p1 = struct {
		name string
		age  int
	}{age: 22, name: "wick"}
	var p2 = struct {
		name string
		age  int
	}{name: "ethan", age: 24}
	fmt.Println("p1:", p1)
	fmt.Println("p2:", p2)
	fmt.Println("")

	type person2 struct {
		name string `tag1`
		age  int    `tag2`
	}
	var person2Inst = person2{"name1", 23}
	fmt.Println("person2Inst:", person2Inst)
}
