package main

import (
	f "fmt"

	"gitlab.com/harvan/go-learn/library"
)

func main() {
	func1()
	func2()
	func3()
	func4()
	func5()
	func6()
}

// penggunaan package, import, dan modifier public & private
func func1() {
	f.Println("func1()...")
	library.SayHello("ethan")
}

// penggunaan public & private pada struct dan propertinya
func func2() {
	f.Println("\nfunc2()...")
	var s1 = library.Student{"ethan", 21}
	f.Println("name ", s1.Name)
	f.Println("grade ", s1.Grade)
}

// import dengan prefix tanda titik
func func3() {
}

// pemanfaatan alias ketika import package
func func4() {
	f.Println("\nfunc4()...")
}

// mengakses properti dalam file yang package-nya sama
func func5() {
	f.Println("\nfunc5()...")
	f.Println("calling... go run bab25.go bab25a.go")
	sayHello("ethan")
}

func func6() {
	f.Println("\nfunc6()...")

	f.Printf("Name\t: %s\n", library.StudentVar.Name)
	f.Printf("Grade\t: %d\n", library.StudentVar.Grade)
}
