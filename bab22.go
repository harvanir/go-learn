package main

import (
	"fmt"
)

func main() {
	func1()
	func2()
	func3()
}

// penerapan pointer
func func1() {
	fmt.Println("func1()...")

	var numberA = 4
	var numberB = &numberA

	fmt.Println("numberA (value)	:", numberA)
	fmt.Println("numberA (address)	:", &numberA)

	fmt.Println("numberB (value)	:", *numberB)
	fmt.Println("numberB (address)	:", numberB)
}

// efek perubahan nilai pointer
func func2() {
	fmt.Println("\nfunc2()...")

	var numberA = 4
	var numberB = &numberA
	var numberC = numberA

	fmt.Println("numberA (value)	:", numberA)
	fmt.Println("numberA (address)	:", &numberA)
	fmt.Println("numberB (value)	:", *numberB)
	fmt.Println("numberB (address)	:", numberB)

	fmt.Println("")

	numberA = 5

	fmt.Println("numberA (value)	:", numberA)
	fmt.Println("numberA (address)	:", &numberA)
	fmt.Println("numberB (value)	:", *numberB)
	fmt.Println("numberB (address)	:", numberB)
	fmt.Println("numberC (value)	:", numberC)
	fmt.Println("numberC (address)	:", &numberC)
}

// parameter pointer
func func3() {
	fmt.Println("\nfunc3()...")

	var number = 4
	fmt.Println("before	:", number)

	change(&number, 10)
	fmt.Println("after	:", number)
}

func change(original *int, value int) {
	*original = value
}
