package main

import (
	"fmt"
	"strings"
)

func main() {
	func1()
	func2()
}

type student struct {
	name  string
	grade int
}

func (s student) sayHello() {
	fmt.Println("halo", s.name)
}

func (s student) getNameAt(i int) string {
	return strings.Split(s.name, " ")[i-1]
}

// penerapan method
func func1() {
	fmt.Println("func1()...")

	var s1 = student{"john wick", 21}
	s1.sayHello()

	var name = s1.getNameAt(2)
	fmt.Println("nama panggilan:", name)
}

type student1 struct {
	name  string
	grade int
}

func (s student1) changeName1(name string) {
	fmt.Println("---> on changeName1, name change to", name)
	s.name = name
}

func (s *student1) changeName2(name string) {
	fmt.Println("---> on changeName2, name change to", name)
	s.name = name
}

// method pointer
func func2() {
	var s1 = student1{"john wick", 21}
	fmt.Println("s1 before", s1.name)
	fmt.Println("")

	s1.changeName1("jason bourne")
	fmt.Println("s1 after changeName1", s1.name)
	fmt.Println("")

	s1.changeName2("ethan hunt")
	fmt.Println("s1 after changeName2", s1.name)
}
