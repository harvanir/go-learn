package main

import (
	"fmt"
	"os"
)

func main() {
	func1()
	func2()
	func3()
}

// penerapan keyword defer
func func1() {
	// finally block
	defer func() {
		fmt.Println("halo")
	}()
	fmt.Println("func1()...")

	fmt.Println("selamat datang")
}

// defer 2
func func2() {
	fmt.Println("\nfunc2()...")

	orderSomeFood("pizza")
	orderSomeFood("burger")
}

func orderSomeFood(menu string) {
	defer fmt.Println("terima kasih, silahkan menunggu")

	if menu == "pizza" {
		fmt.Print("pilihan tepat!", " ")
		fmt.Print("pizza di tempat kami paling enak!", "\n")
		return
	}

	fmt.Println("pesanan anda:", menu)
}

// penerapan fungsi os.Exit()
func func3() {
	fmt.Println("\nfunc3()...")
	defer fmt.Println("halo")
	os.Exit(1)
	fmt.Println("selamat datang")
}
