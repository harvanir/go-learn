package main

import (
	"fmt"
	"math"
)

func main() {
	func1()
	func2()
}

// penerapan fungsi multiple return
func func1() {
	fmt.Println("\func1()...")
	var diameter float64 = 15
	var area, circumreference = calculate(diameter)

	fmt.Printf("area: %f, circumreference: %f\n", area, circumreference)
}

func calculate(d float64) (float64, float64) {
	// hitung luas
	var area = math.Pi * math.Pow(d/2, 2)

	// hitung keliling
	var circumreference = math.Pi * d

	// kembalikan 2 nilai
	return area, circumreference
}

// fungsi dengan predefined return value
func func2() {
	fmt.Println("\func2()...")

	var diameter float64 = 15
	var area, circumreference = calculate2(diameter)

	fmt.Printf("area: %f, circumreference: %f\n", area, circumreference)
}

func calculate2(d float64) (area float64, circumreference float64) {
	// hitung luas
	area = math.Pi * math.Pow(d/2, 2)

	// hitung keliling
	circumreference = math.Pi * d

	return
}
